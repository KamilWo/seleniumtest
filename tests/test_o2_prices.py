# -*- coding: utf-8 -*-

"""Testing CallPrice class
"""

import pytest
from core.o2_prices import CallPrice, __url__, __expectedSiteTitle__


def create_CallPrice(country):
    global __url__
    global __expectedSiteTitle__
    return CallPrice(url=__url__, country=country)


class Test_CallPrice_Canada:
    call_price = create_CallPrice('Canada')

    def test_landline_canada(self):
        assert self.call_price.get_land_price() == u'£1.00'

    def test_mobile_canada(self):
        assert self.call_price.get_land_price() == u'£1.00'


class Test_CallPrice_Germany:
    call_price = create_CallPrice('Germany')

    def test_landline_Germany(self):
        assert self.call_price.get_land_price() == u'£1.00'

    def test_mobile_Germany(self):
        assert self.call_price.get_land_price() == u'£1.00'


class Test_CallPrice_Iceland:
    call_price = create_CallPrice('Iceland')

    def test_landline_Iceland(self):
        assert self.call_price.get_land_price() == u'£1.00'

    def test_mobile_Iceland(self):
        assert self.call_price.get_land_price() == u'£1.00'


class Test_CallPrice_Pakistan:
    call_price = create_CallPrice('Pakistan')

    def test_landline_Pakistan(self):
        assert self.call_price.get_land_price() == u'£1.20'

    def test_mobile_Pakistan(self):
        assert self.call_price.get_land_price() == u'£1.20'


class Test_CallPrice_Singapore:
    call_price = create_CallPrice('Singapore')

    def test_landline_Singapore(self):
        assert self.call_price.get_land_price() == u'£1.00'

    def test_mobile_Singapore(self):
        assert self.call_price.get_land_price() == u'£1.00'


class Test_CallPrice_South_Africa:
    call_price = create_CallPrice('South Africa')

    def test_landline_South_Africa(self):
        assert self.call_price.get_land_price() == u'£1.00'

    def test_mobile_South_Africa(self):
        assert self.call_price.get_land_price() == u'£1.00'
