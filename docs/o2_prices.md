# O2 prices

Please make sure of installing requirements and chromedriver:

        pip install -r requirements.txt

        https://sites.google.com/a/chromium.org/chromedriver/downloads

Tests can be run by using command:

        py.test teststest_o2_prices.py

Application can be run by using command depending on OS:

        python core\o2_prices.py

or

        python core/o2_prices.py

