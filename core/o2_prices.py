# -*- coding: utf-8 -*-
"""Application for checking international call prices in O2 UK
"""

import urllib2
from urllib2 import URLError
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import traceback


# All available countries in website
__all__ = [
    'Abkhazia', 'Afghanistan', 'Alaska', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Anguilla',
    'Antarctica', 'Antigua and Barbuda', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan',
    'Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan',
    'Bolivia', 'Bosnia', 'Botswana', 'Brazil', 'British Virgin Islands', 'Brunei', 'Bulgaria', 'Burkina Faso',
    'Burundi', 'Cambodia', 'Cameroon', 'Canada', 'Canary Islands', 'Cape Verde', 'Cayman Islands', 'Chad',
    'Central African Rep', 'Chile', 'China', 'Christmas Island', 'Colombia', 'Comoros', 'Congo', 'Cook Islands',
    'Costa Rica', 'Croatia', 'Cuba', 'Cyprus', 'Cyprus, North', 'Czech Republic', 'Congo(DRO)', 'Denmark',
    'Diego Garcia', 'Djibouti', 'Dominica', 'Dominican Republic', 'East Timor', 'Ecuador', 'Egypt', 'El Salvador',
    'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia', 'Falkland Islands', 'Faroe Islands', 'Fiji', 'Finland',
    'France', 'French Guiana', 'French Polynesia', 'Gabon', 'Gambia', 'Georgia', 'Germany', 'Ghana', 'Gibraltar',
    'Greece', 'Greenland', 'Grenada', 'Guadeloupe', 'Guam', 'Guatemala', 'Guernsey', 'Guinea', 'Guinea Bissau',
    'Guyana', 'Haiti', 'Hawaii', 'Honduras', 'Hong Kong', 'Hungary', 'Iceland', 'India', 'Indonesia', 'Iran',
    'Iraq', 'Ireland', 'Isle of Man', 'Israel', 'Italy', 'Ivory Coast', 'Jamaica', 'Japan', 'Jersey', 'Jordan',
    'Kazakhstan', 'Kenya', 'Kiribati', 'Korea, North', 'Korea, South', 'Kosovo', 'Kuwait', 'Kyrgyzstan', 'Laos',
    'Latvia', 'Lebanon', 'Lesotho', 'Liberia', 'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macau',
    'Macedonia', 'Madagascar', 'Malawi', 'Malaysia', 'Maldives', 'Mali', 'Malta', 'Marshall Islands', 'Martinique',
    'Mauritania', 'Mauritius', 'Mayotte', 'Mexico', 'Micronesia', 'Moldova', 'Monaco', 'Mongolia', 'Montenegro',
    'Montserrat', 'Morocco', 'Mozambique', 'Myanmar', 'Namibia', 'Nauru', 'Nepal', 'Netherlands', 'Curacao',
    'New Caledonia', 'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'Niue', 'NLD Antilles', 'Norfolk Island',
    'Northern Marianna Islands', 'Norway', 'Oman', 'Pakistan', 'Palau', 'Palestine', 'Panama', 'Papua New Guinea',
    'Paraguay', 'Peru', 'Philippines', 'Poland', 'Portugal', 'Puerto Rico', 'Qatar', 'Reunion', 'Romania', 'Russia',
    'Rwanda', 'Saint Maarten', 'San Marino', 'Sao Tome & Principe', 'Saudi Arabia', 'Senegal', 'Serbia', 'Seychelles',
    'Sierra Leone', 'Singapore', 'Slovak Rep', 'Slovenia', 'Solomon Islands', 'Somalia', 'South Africa', 'Spain',
    'Sri Lanka', 'St Helena', 'St Kitts', 'St Lucia', 'St Pierre & Miquel.', 'St Vincent', 'Sudan', 'Sudan, South',
    'Suriname', 'Swaziland', 'Sweden', 'Switzerland', 'Syria', 'Taiwan', 'Tajikistan', 'Tanzania', 'Thailand', 'Togo',
    'Tokelau', 'Tonga', 'Trinidad and Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Turks and Caicos', 'Tuvalu',
    'Uganda', 'Ukraine', 'United Arab Emirates(UAE)', 'United States of America(USA)', 'Uruguay', 'US Virgin Islands',
    'Uzbekistan', 'Vanuatu', 'Vatican City', 'Venezuela', 'Vietnam', 'Wallis & Futuna', 'Western Samoa', 'Yemen',
    'Zambia', 'Zimbabwe', 'Ships / ferries / Airplane networks'
]

__testCountries__ = [
    'Canada', 'Germany', 'Iceland', 'Pakistan', 'Singapore', 'South Africa'
]

__url__ = 'http://international.o2.co.uk/internationaltariffs/calling_abroad_from_uk'

__expectedSiteTitle__ = 'O2 | International | International Caller Bolt On'


class CallPrice:
    """Information about landlines and mobile call costs."""

    def __init__(self, url='', country='', expected_title=''):
        """Initialize a CallPrice instance."""
        try:
            req = urllib2.Request(url)
            urllib2.urlopen(req)
            self._url = url
        except URLError, e:
            print('Init failed {}'.format(e.reason))

        self._land_price = 0.0
        self._mobile_price = 0.0

        browser = webdriver.Chrome()
        browser.get(url)
        if expected_title in browser.title:
            countrySearch = browser.find_element(By.ID, 'countryName')  # Find the search box
            countrySearch.send_keys(country + Keys.RETURN)
            wait_for_element = WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((By.ID, "standardRatesTable"))
            )
            landPrice = browser.find_element(By.XPATH, '//*[@id="standardRatesTable"]/tbody/tr[1]/td[2]')  # Find the landPrice
            self._land_price = get_text_excluding_children(browser, landPrice)
            mobilePrice = browser.find_element(By.XPATH, '//*[@id="standardRatesTable"]/tbody/tr[2]/td[2]')  # Find the mobilePrice
            self._mobile_price = get_text_excluding_children(browser, mobilePrice)
            browser.quit()
        else:
            browser.quit()
            print 'Not exactly what we expected, driver error...'
            return

    def get_land_price(self):
        return self._land_price

    def get_mobile_price(self):
        return self._mobile_price


def get_text_excluding_children(driver, element):
    """Taking text from node element
    """
    return driver.execute_script("""
    return jQuery(arguments[0]).contents().filter(function() {
        return this.nodeType == Node.TEXT_NODE;
    }).text();
    """, element)


def print_test_prices():
    """Printing test prices
    """
    print '\n================================\n|    Calling prices in O2:     |\n================================\n'
    for country in __testCountries__:
        try:
            current_price = CallPrice(url=__url__, country=country)
            print u"""{}\n\n    Landline price:  {}\n\n    Mobile price:    {}\n\n""".format(
                country,
                current_price.get_land_price(),
                current_price.get_mobile_price()
            )
        except Exception, e:
            traceback.print_exc(e)
            print 'Pricing failed for country {}'.format(country)

    print '\n\n=== Thank you for using our services. Good bye. ==='

if __name__ == '__main__':
    print_test_prices()

